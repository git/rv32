"""
/*
 * Copyright 2018 Jacob Lifshay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
`timescale 1ns / 1ps
`include "riscv.vh"
`include "cpu.vh"
"""

import string
from migen import *
from migen.fhdl import verilog
from migen.fhdl.structure import _Operator

from riscvdefs import *
from cpudefs import *

class CPUMIP(Module):
    def __init__(self):
        Module.__init__(self)
        # TODO: implement ext interrupts
        self.meip = Signal(name="mip_meip", reset=0)
        self.seip = Signal(name="mip_seip", reset=0)
        self.ueip = Signal(name="mip_uiep", reset=0)
        # TODO: implement timer interrupts
        self.mtip = Signal(name="mip_mtip", reset=0)
        self.stip = Signal(name="mip_stip", reset=0)
        self.msip = Signal(name="mip_stip", reset=0)
        self.utip = Signal(name="mip_utip", reset=0)
        self.ssip = Signal(name="mip_ssip", reset=0)
        self.usip = Signal(name="mip_usip", reset=0)

        self.mip = Signal(32)
        self.comb += self.mip.eq(self.make())

    def make(self):
        return Cat( self.usip, self.ssip, 0, self.msip,
                    self.utip, self.stip, 0, self.mtip,
                    self.ueip, self.seip, 0, self.meip, )


if __name__ == "__main__":
    example = CPUMIP()
    print(verilog.convert(example,
         {
           example.mip,
           }))
