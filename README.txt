# Limitations

* there is no << or >> operator, only <<< and >>> (arithmetic shift)
  _Operator("<<", [lhs, rhs]) will generate verilog however simulation
  will fail, and value_bits_sign will not correctly recognise it
* it is not possible to declare parameters
* an input of [31:2] is not possible, only a parameter of [N:0]
* tasks are not supported.
* Clock Domains: https://gist.github.com/cr1901/5de5b276fca539b66fe7f4493a5bfe7d
