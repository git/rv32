"""
/*
 * Copyright 2018 Jacob Lifshay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
`timescale 1ns / 1ps
`include "riscv.vh"
`include "cpu.vh"
"""

import string
from migen import *
from migen.fhdl import verilog
from migen.fhdl.structure import _Operator

from riscvdefs import *
from cpudefs import *

class CPUMIE(Module):
    def __init__(self):
        Module.__init__(self)
        self.meie = Signal(name="mie_meie", reset=0)
        self.mtie = Signal(name="mie_mtie", reset=0)
        self.msie = Signal(name="mie_msie", reset=0)
        self.seie = Signal(name="mie_seie")
        self.ueie = Signal(name="mie_ueie")
        self.stie = Signal(name="mie_stie")
        self.utie = Signal(name="mie_utie")
        self.ssie = Signal(name="mie_ssie")
        self.usie = Signal(name="mie_usie")

        for n in dir(self):
            if n.startswith("_"):
                continue
            n = getattr(self, n)
            if not isinstance(n, Signal):
                continue
            self.comb += n.eq(0x0)

        self.mie = Signal(32)

        self.sync += self.mie.eq(self.make())

    def make(self):
        return Cat( self.usie, self.ssie, 0, self.msie,
                    self.utie, self.stie, 0, self.mtie,
                    self.ueie, self.seie, 0, self.meie, )



if __name__ == "__main__":
    example = CPUMIE()
    print(verilog.convert(example,
         {
           example.meie,
           example.mtie,
           example.msie,
           example.mie,
           }))
